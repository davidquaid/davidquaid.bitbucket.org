//var requirejs = require('requirejs');

// var AFRAME = require('aframe');
// var stereoComponent = require('aframe-stereo-component').stereo_component;
// var stereocamComponent = require('aframe-stereo-component').stereocam_component;

// AFRAME.registerComponent('stereo', stereoComponent);
// AFRAME.registerComponent('stereocam', stereocamComponent);


//var image = document.images[0];

let dataArray = 
    [//{"url":"https://farm6.staticflickr.com/5491/14522860236_4db6277e57_b.jpg"},{"url":"https://farm9.staticflickr.com/8847/28288591186_0b5ca9315f_b.jpg"},{"url":"https://farm4.staticflickr.com/3893/14485001657_acf55f4604_b.jpg"},

	{"id":"33128892956","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/691/33128892956_2628730e95_m.jpg","metadata":"Loch Lomond Swan Island, near Luss"},{"id":"32355160793","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3689/32355160793_60665d575f_m.jpg","metadata":"Edinburgh, from Calton Hill"},{"id":"33128892606","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/750/33128892606_0617136ced_m.jpg","metadata":"Edinburgh Castle from the Grassmarket"},{"id":"33128892386","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3809/33128892386_ef617d5cee_m.jpg","metadata":"Ellen's Isle, Loch Katrine"},{"id":"33128892156","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/747/33128892156_48e42ebf60_m.jpg","metadata":"Abbotsford - Room in which Sir Walter Scott Died"},{"id":"32355160383","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3812/32355160383_73524eb072_m.jpg","metadata":"Edinburgh, from the Castle"},{"id":"33128891776","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3774/33128891776_f0fb719b72_m.jpg","metadata":"Ellen's Isle, from the Goblin's Cave"},{"id":"32355160053","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3718/32355160053_e1c3602fa3_m.jpg","metadata":"Loch Achray and Ben Venue"},{"id":"33014046582","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm3.staticflickr.com/2858/33014046582_70a43c6d75_m.jpg","metadata":"Balmoral Castle, from the South East"},{"id":"33042916191","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm3.staticflickr.com/2886/33042916191_265da22a80_m.jpg","metadata":"Glasgow Necropolis"},{"id":"33014046092","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/296/33014046092_82ca76d0ae_m.jpg","metadata":"Castle Street, Aberdeen"},{"id":"33042915881","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/774/33042915881_828ccc0447_m.jpg","metadata":"Union Bridge, Aberdeen"},{"id":"33014045772","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/590/33014045772_8bd7f1bf0c_m.jpg","metadata":"Aberdeen, Looking towards Girdleness"},{"id":"33128890496","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/765/33128890496_c9e97a482c_m.jpg","metadata":"Edinburgh Castle from the Grassmarket"},{"id":"32325290464","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3871/32325290464_ce918037a7_m.jpg","metadata":"Invertrossachs, Loch Vennachar"},{"id":"33042915281","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/727/33042915281_26e905bb27_m.jpg","metadata":"Hermitage Bridge, Dunkeld"},{"id":"32355159403","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3857/32355159403_c7e38e9175_m.jpg","metadata":"Glasgow Cathedral - The Choir"},{"id":"33042914741","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3708/33042914741_23ba443ae3_m.jpg","metadata":"Ballater, Aberdeenshire"},{"id":"33042914611","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/671/33042914611_3f184d9468_m.jpg","metadata":"Pass of Killiecrankie, looking Down"},{"id":"33042914051","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3716/33042914051_a84ec5843f_m.jpg","metadata":"Loch Katrine"},{"id":"33042914371","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm3.staticflickr.com/2847/33042914371_cdaa59a1c0_m.jpg","metadata":"Carlonan Bridge, Inverary"},{"id":"33042913801","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/599/33042913801_fbdc98deab_m.jpg","metadata":"The Queen's View, Pass of Killiecrankie"},{"id":"33042913501","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm3.staticflickr.com/2849/33042913501_32ab9428fc_m.jpg","metadata":"Glen Gyle - Rob Roy's Country, Loch Katrine"},{"id":"33042913151","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3767/33042913151_2fd63442af_m.jpg","metadata":"Pass of Killiecrankie, looking up"},{"id":"33042912621","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/782/33042912621_7c9fffef1b_m.jpg","metadata":"Abbotsford, from the River"},{"id":"33042912891","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3722/33042912891_2dd49e8b21_m.jpg","metadata":"Fall on the Braan at the Hermitage Bridge, Dunkeld"},{"id":"32788069160","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3891/32788069160_e770317175_m.jpg","metadata":"Rob Roy's Prison - Stronaclachar, Loch Katrine"},{"id":"33042912141","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm3.staticflickr.com/2914/33042912141_8f5879967f_m.jpg","metadata":"Balmoral Castle, from the South West"},{"id":"33042912351","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/651/33042912351_1943fbabaa_m.jpg","metadata":"Princes Street, Edinburgh"},{"id":"33042911761","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3922/33042911761_1cdc2b3a40_m.jpg","metadata":"Sunset at the Loch of Park, Aberdeenshire"},{"id":"33042911431","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3760/33042911431_aa7ca1447c_m.jpg","metadata":"A Bay on Loch Lomond, near Luss"},{"id":"33042911551","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3940/33042911551_600d269f03_m.jpg","metadata":"The Sir Walter Scott Monument, Edinburgh"},{"id":"33042911021","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3668/33042911021_15fae294b0_m.jpg","metadata":"Abbotsford, From the South East"},{"id":"33042911191","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm3.staticflickr.com/2823/33042911191_aa8560605f_m.jpg","metadata":"Aberdeen University"},{"id":"33042910811","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/665/33042910811_6ba5540c29_m.jpg","metadata":"The Market Cross, Aberdeen"},{"id":"33042910501","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3685/33042910501_08d9b933e9_m.jpg","metadata":"Melrose Abbey, from the South West"},{"id":"33042910321","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3923/33042910321_33d4115983_m.jpg","metadata":"Stronachlachar, Loch Katrine"},{"id":"32858791102","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/454/32858791102_db68053585_m.jpg","metadata":"Dumbarton Rock and Castle"},{"id":"32632354860","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/781/32632354860_62fea641a0_m.jpg","metadata":"Studies on Ryde Pier - Evening"},{"id":"32972824096","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm3.staticflickr.com/2758/32972824096_950b63f992_m.jpg","metadata":"Kyle of Bute, looking east"},{"id":"32972822866","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/694/32972822866_fe4b6f2ae7_m.jpg","metadata":"Mer de glace"},{"id":"3102710286","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3049/3102710286_83e6a600fe_m.jpg","metadata":"'Looking through the Great Forth Bridge (8,300 feet long), Scotland'"},{"id":"32858790432","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm4.staticflickr.com/3953/32858790432_66abe9332b_m.jpg","metadata":"'Fall at Inversnaid, Loch Lomond'"},{"id":"32632354310","xid":8,"owner":"From: National Galleries of Scotland","url":"https://farm1.staticflickr.com/285/32632354310_f3f1a9eb14_m.jpg","metadata":"Mer de glace"}];


let index = 0;
AFRAME.registerComponent('sg', {
    schema: {type: 'string'},
    init: function () {
	var stringToLog = this.data;
	console.log(stringToLog);

	this.el.addEventListener('click', this.advanceCard);
	this.advanceCard();
    },
    advanceCard: function() {
	
	let canvasL = document.querySelector('#left-canvas');
	let canvasR = document.querySelector('#right-canvas');

	let description = document.querySelector('#description');
	
	let card = dataArray[index];
	index = (index + 1) % dataArray.length;

	var downloadingImage = new Image();
	downloadingImage.crossOrigin = 'Anonymous';
	downloadingImage.onload = function() {
	    canvasL.height = downloadingImage.height;
	    canvasL.width = downloadingImage.width / 2;
	    canvasR.height = downloadingImage.height;
	    canvasR.width = downloadingImage.width / 2;

	    // draw the left eye:
	    canvasL.getContext('2d').drawImage(downloadingImage, 0, 0, canvasL.width, canvasL.height, 0, 0, canvasL.width, canvasL.height);
	    // Draw the right eye
            canvasR.getContext('2d').drawImage(downloadingImage, canvasR.width, 0, canvasR.width, canvasR.height, 0, 0, canvasR.width, canvasR.height);
	};
	
	downloadingImage.src = card.url.replace('m.jpg', 'b.jpg');

	description.setAttribute('value', (index == 0 ? dataArray.length : index).toString() + "/" + dataArray.length.toString() + "\n" +  card.metadata);

  }
});

