let index = 0;
let Z_MAX = 10;
let Z_MIN = -2;
let DELTA = 0.5;
let INSTRUCTIONS =
  '\nTap screen, or click while focused on the image to advance.';
AFRAME.registerComponent('sg', {
  schema: { type: 'string' },
  init: function() {
    var stringToLog = this.data;
    console.log(stringToLog);

    this.setMonoMode(!this.isInVrMode());
    this.l = document.querySelector('#l');
    this.r = document.querySelector('#r');

    this.canvasL = document.querySelector('#left-canvas');
    this.canvasR = document.querySelector('#right-canvas');

    // Update the image being displayed when entering and exiting VR
    // mode.  Advance by 0 does this, since it checks the state of
    // vr-mode on the scene.
    this.el.sceneEl.addEventListener(
      'enter-vr',
      function() {
        console.log('Entering VR mode');
        this.setMonoMode(false);
      }.bind(this)
    );
    this.el.sceneEl.addEventListener(
      'exit-vr',
      function() {
        console.log('Exiting VR mode');
        this.setMonoMode(true);
      }.bind(this)
    );

    let cursor = document.querySelector('#cursor');
    l.addEventListener('click', this.advanceCard.bind(this));
    l.addEventListener('mouseenter', function(event) {
      // Keep the cursor invisible over the left eye.
      cursor.setAttribute('visible', false);
      // Make sure the right eye gets the memo about the mouse movement!
      r.dispatchEvent(new MouseEvent(event.type, event));
    });
    l.addEventListener('mouseleave', function(event) {
      r.dispatchEvent(new MouseEvent(event.type, event));
    });
    l.addEventListener('mouseleave', function(event) {
      cursor.setAttribute('visible', true);
    });
    document
      .querySelector('#zoom_in')
      .addEventListener('click', this.zoom_in.bind(this));
    document
      .querySelector('#zoom_out')
      .addEventListener('click', this.zoom_out.bind(this));
    document
      .querySelector('#prev')
      .addEventListener('click', this.prev.bind(this));
    document
      .querySelector('#mono_toggle')
      .addEventListener('click', this.monoToggle.bind(this));

    // Okay, parse the params!
    let queryParams = getJsonFromUrl(false);
    if (queryParams.id) {
      let id = queryParams.id;
      let database = firebase.database();
      let galleryRef = database.ref(`galleries/${id}`);
      galleryRef.once('value').then(
        function(lookupResult) {
          if (lookupResult.val()) {
            this.setupCollection(lookupResult.val());
          } else {
            console.log(`Invalid id: ${lookupResult.getKey()}`);
          }
        }.bind(this)
      );
    }
  },
  setupCollection: function(collection) {
    let title = document.querySelector('#title');
    title.setAttribute('value', collection.title + INSTRUCTIONS);
    this.dataArray = collection.records;
    this.advanceBy(0);
  },
  advanceCard: function(event) {
    this.advanceBy(1);
  },
  advanceBy: function(offset) {
    if (!this.dataArray) {
      return;
    }
    let adjustedIndex = index + offset;
    index =
      (adjustedIndex % this.dataArray.length + this.dataArray.length) %
      this.dataArray.length;
    let card = this.dataArray[index];
    let description = document.querySelector('#description');

    var downloadingImage = new Image();
    downloadingImage.crossOrigin = 'Anonymous';
    downloadingImage.onload = function() {
      let cL = this.canvasL;
      let cR = this.canvasR;

      const aspectRatio = this.shouldShowFullImage()
        ? downloadingImage.width / downloadingImage.height
        : downloadingImage.width / 2 / downloadingImage.height;
      let existingScale = l.getAttribute('scale');
      l.setAttribute('scale', {
        x: aspectRatio,
        y: existingScale.y,
        z: existingScale.z
      });
      let existingScaleR = r.getAttribute('scale');
      r.setAttribute('scale', {
        x: aspectRatio,
        y: existingScaleR.y,
        z: existingScaleR.z
      });

      if (this.shouldShowFullImage()) {
        // draw the left eye:
        cL.height = downloadingImage.height;
        cL.width = downloadingImage.width;
        cR.height = downloadingImage.height;
        cR.width = downloadingImage.width;
        cL
          .getContext('2d')
          .drawImage(
            downloadingImage,
            0,
            0,
            cL.width,
            cL.height,
            0,
            0,
            cL.width,
            cL.height
          );
        // Draw the right eye
        cR
          .getContext('2d')
          .drawImage(
            downloadingImage,
            0,
            0,
            cR.width,
            cR.height,
            0,
            0,
            cR.width,
            cR.height
          );
      } else {
        cL.height = downloadingImage.height;
        cL.width = downloadingImage.width / 2;
        cR.height = downloadingImage.height;
        cR.width = downloadingImage.width / 2;
        // draw the left eye:
        cL
          .getContext('2d')
          .drawImage(
            downloadingImage,
            0,
            0,
            cL.width,
            cL.height,
            0,
            0,
            cL.width,
            cL.height
          );
        // Draw the right eye
        cR
          .getContext('2d')
          .drawImage(
            downloadingImage,
            cR.width,
            0,
            cR.width,
            cR.height,
            0,
            0,
            cR.width,
            cR.height
          );
      }
    }.bind(this);

    downloadingImage.src = card.url; //.replace('m.jpg', 'b.jpg');

    description.setAttribute(
      'value',
      (index + 1).toString() +
        '/' +
        this.dataArray.length.toString() +
        '\n' +
        card.description
    );
  },
  next: function() {
    this.advanceBy(1);
  },
  prev: function() {
    this.advanceBy(-1);
  },
  zoom_in: function(event) {
    this.zoom_by(-DELTA);
  },
  zoom_out: function(event) {
    this.zoom_by(DELTA);
  },
  zoom_by: function(delta) {
    let camera = document.querySelector('a-camera');
    let animation = camera.getAttribute('animation__template');
    let position = camera.getAttribute('position');
    let newPosition = {
      x: position.x,
      y: position.y,
      z: Number.parseFloat(position.z) + delta
    };
    animation.to = `${newPosition.x} ${newPosition.y} ${newPosition.z}`;
    animation.startEvents = ['zoom-event'];
    if (newPosition.z < Z_MAX && newPosition.z > Z_MIN) {
      //      camera.setAttribute('position', newPosition);
      camera.setAttribute('animation__next', animation);
      let event = new Event('zoom-event');
      camera.dispatchEvent(event);
    }
  },
  shouldShowFullImage: function() {
    return this.monoMode;
  },
  isInVrMode: function() {
    return this.el.sceneEl.is('vr-mode');
  },
  setMonoMode: function(value) {
    this.monoMode = value;
    let mono = document.querySelector('#mono_toggle');
    let material = mono.getAttribute('material');
    let icon = value ? 'glasses' : 'mono_view';
    material.src = `assets/${icon}.png`;
    mono.setAttribute('material', material);
    this.advanceBy(0);
  },
  monoToggle: function() {
    this.setMonoMode(!this.monoMode);
  }
});
