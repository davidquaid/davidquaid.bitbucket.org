/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Initializes StereoGallery.
function StereoGallery() {
    this.checkSetup();
    this.shortenerApiKey = 'AIzaSyChiyBAzX3Xtz1tkMTgUAn3b0M09nq14v4';

  // Shortcuts to DOM Elements.
  // this.messageList = document.getElementById('messages');
  // this.messageForm = document.getElementById('message-form');
  // this.messageInput = document.getElementById('message');
  // this.submitButton = document.getElementById('submit');
  // this.submitImageButton = document.getElementById('submitImage');
  // this.imageForm = document.getElementById('image-form');
  // this.mediaCapture = document.getElementById('mediaCapture');
  this.userPic = document.getElementById('user-pic');
  this.userName = document.getElementById('user-name');
  this.signInButton = document.getElementById('sign-in');
  this.signOutButton = document.getElementById('sign-out');
  this.signInSnackbar = document.getElementById('must-signin-snackbar');

  this.allInputs = document.querySelector('#inputs');
  this.createButton = document.getElementById('create')
    .addEventListener('click', this.createGallery.bind(this));;  
  this.resultDiv = document.getElementById('result');
  this.title = document.getElementById('title');
  this.publicToggle = document.querySelector('#keep-collection-private');

  // Saves message on form submit.
  // this.messageForm.addEventListener('submit', this.saveMessage.bind(this));
  this.signOutButton.addEventListener('click', this.signOut.bind(this));
  this.signInButton.addEventListener('click', this.signIn.bind(this));

  // Toggle for the button.
  // var buttonTogglingHandler = this.toggleButton.bind(this);
  // this.messageInput.addEventListener('keyup', buttonTogglingHandler);
  // this.messageInput.addEventListener('change', buttonTogglingHandler);

  // Events for image upload.
  // this.submitImageButton.addEventListener('click', function(e) {
  //   e.preventDefault();
  //   this.mediaCapture.click();
  // }.bind(this));
  // this.mediaCapture.addEventListener('change', this.saveImageMessage.bind(this));

  this.initFirebase();
  // Data ref to the gallery selected: When assigned, it means that a
  // load has already taken place, or is in progress.
  this.ref = null;
  this.initPage();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
StereoGallery.prototype.initFirebase = function() {
  // Shortcuts to Firebase SDK features.
  this.auth = firebase.auth();
  this.database = firebase.database();
  // Initiates Firebase auth and listen to auth state changes.
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

StereoGallery.prototype.initPage = function() {
  if (this.ref != null) {
    return;
  }
  let queryParams = getJsonFromUrl(false);
  if (queryParams.hasOwnProperty('id')) {
    this.ref = this.database.ref(`galleries/${queryParams.id}`);
    this.ref.once('value').then(function (lookupResult) {
      if (lookupResult.val()) {
	if (!this.auth.currentUser||
	    (this.auth.currentUser.uid != lookupResult.val().uid)) {
	  this.showSnackbar('Only the original author can edit a collection.');
	  this.ref = null;
	  return;
	}
	this.setupCollection(lookupResult.val());
      } else {
	this.showSnackbar('Invalid collection id');
	this.addInputIfNecessary(null);
      }
    }.bind(this));
  } else {
    this.addInputIfNecessary(null);
  }
};

StereoGallery.prototype.setupCollection = function(collection) {
  // DO(davidquaid): Also, set up instructions at the top of the page.
  
  this.title.value = collection.title;
  for (let record of collection.records) {
    this.addInputNode(record);
  }
  this.displayCreationStatus(collection, 'last modified');
};

// Loads chat messages history and listens for upcoming ones.
StereoGallery.prototype.loadMessages = function() {
  // Reference to the /messages/ database path.
  this.messagesRef = this.database.ref('messages');
  // Make sure we remove all previous listeners.
  this.messagesRef.off();

  // Loads the last 12 messages and listen for new ones.
  var setMessage = function(data) {
    var val = data.val();
    this.displayMessage(data.key, val.name, val.text, val.photoUrl, val.imageUrl);
  }.bind(this);
  this.messagesRef.limitToLast(12).on('child_added', setMessage);
  this.messagesRef.limitToLast(12).on('child_changed', setMessage);
};

// Saves a new message on the Firebase DB.
StereoGallery.prototype.saveMessage = function(e) {
  e.preventDefault();
  // Check that the user entered a message and is signed in.
  if (this.messageInput.value && this.checkSignedInWithMessage()) {
    var currentUser = this.auth.currentUser;
    // Add a new message entry to the Firebase Database.
    this.messagesRef.push({
      name: currentUser.displayName,
      text: this.messageInput.value,
      photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
    }).then(function() {
      // Clear message text field and SEND button state.
      StereoGallery.resetMaterialTextfield(this.messageInput);
      this.toggleButton();
    }.bind(this)).catch(function(error) {
      console.error('Error writing new message to Firebase Database', error);
    });
  }
};

// Sets the URL of the given img element with the URL of the image stored in Cloud Storage.
StereoGallery.prototype.setImageUrl = function(imageUri, imgElement) {
  // If the image is a Cloud Storage URI we fetch the URL.
  if (imageUri.startsWith('gs://')) {
    imgElement.src = StereoGallery.LOADING_IMAGE_URL; // Display a loading image first.
    this.storage.refFromURL(imageUri).getMetadata().then(function(metadata) {
      imgElement.src = metadata.downloadURLs[0];
    });
  } else {
    imgElement.src = imageUri;
  }
};

let STEREO_RECORD = 1;
let TEXT_RECORD = 2;
StereoGallery.prototype.createGallery = function(event) {
    event.preventDefault();
    let urls = document.querySelectorAll('#url');
    let descriptions = document.querySelectorAll('#description');
    let attributions = document.querySelectorAll('#attribution');

    let stereographRecords = [];
    let i = 0;
    for (let url of urls) {
	if (url.value !== '') {
	    stereographRecords.push(
		{
		    url: url.value,
		    description: descriptions[i].value,
		    attribution: attributions[i].value,
		    type: STEREO_RECORD
		});
	}
	++i;
    }
    let collection = {
      'title': this.title.value,
      'description': '',
      'records': stereographRecords,
      'owner-private': !this.publicToggle.checked
    };

  if (stereographRecords.length == 0) {
    this.showSnackbar('You need to add at least one stereograph.');
  } else {  
    this.saveCollection(collection);
  }
};


// Saves a new message containing an image URI in Firebase.
// This first saves the image in Firebase storage.
StereoGallery.prototype.saveCollection = function(collection) {
  event.preventDefault();

    if (title.value.trim() === '') {
	var data = {
	    message: 'Your collection must have a title.',
	    timeout: 4000
	};
    this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
    return;
  }

  // Check if the user is signed-in
  if (this.checkSignedInWithMessage()) {
      let currentUser = this.auth.currentUser;
      let galleriesListRef = this.database.ref('galleries');
    collection.uid = currentUser.uid;
    let utcModTime = Date.now();
    collection.lastModified = utcModTime;
    if (!collection.created) {
      collection.created = utcModTime;
    }
      collection.userName = currentUser.displayName;
      collection.userPhotoUrl = currentUser.photoURL;
    let galleryRef = this.ref == null ? galleriesListRef.push() : this.ref;
      console.log(collection);

      let newUrl = `https://davidquaid.bitbucket.io/galleries/?id=${galleryRef.getKey()}`;
      
      fetch('https://www.googleapis.com/urlshortener/v1/url?key=' + this.shortenerApiKey, {
	  method: 'post',
	  headers: new Headers({
	      'Content-Type': 'application/json'
	  }),
	  body: `{"longUrl": "${newUrl}"}`
      }).then(function(response) {
	  return response.json();
      }.bind(this)).then(function(json) {
	console.log(json);
	collection.shortUrl = json.id;
	galleryRef.set(collection);

	let action = this.ref == null ? 'created' : 'updated';
	this.ref = galleryRef;
	this.displayCreationStatus(collection, action);

      }.bind(this));
  }
};

StereoGallery.prototype.displayCreationStatus = function(collection, action) {
  let r = document.querySelector('#result-message');

  let updateLink = window.location.href;
  if (updateLink.endsWith('html')) {
    updateLink = updateLink + `?id=${this.ref.getKey()}`;
  }

  let lastModified = new Date(collection.lastModified);
  let shortLinkHtml = `Your collection was ${action} at ${lastModified.toLocaleTimeString()}, share this collection with this link: <a href="${collection.shortUrl}">${collection.shortUrl}</a><br />
A smartphone with Google Chrome and a <a href="http://g.co/cardboard">Cardboard</a> compatible viewer is recommended.<br />
        You can always come back to edit your collection through <a href=${updateLink}>this link</a>, or scan the qr code below.`;
  r.innerHTML = shortLinkHtml;
  
  // The short link is, kindly, constant for the same collection.  But let's make sure
  // to only add a qr code to the div if one is not already there.
  let qrDiv = document.querySelector('#qrcode');
  if (qrDiv.childElementCount == 0) {
    new QRCode(qrDiv, collection.shortUrl);
  }
};

// Saves a new message containing an image URI in Firebase.
// This first saves the image in Firebase storage.
StereoGallery.prototype.saveImageMessage = function(event) {
  event.preventDefault();
  var file = event.target.files[0];

  // Clear the selection in the file picker input.
  this.imageForm.reset();

  // Check if the file is an image.
  if (!file.type.match('image.*')) {
    var data = {
      message: 'You can only share images',
      timeout: 2000
    };
    this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
    return;
  }

  // Check if the user is signed-in
  if (this.checkSignedInWithMessage()) {

    // We add a message with a loading icon that will get updated with the shared image.
    var currentUser = this.auth.currentUser;
          this.messagesRef.push({
      name: currentUser.displayName,
      imageUrl: StereoGallery.LOADING_IMAGE_URL,
      photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
    }).then(function(data) {

      // Upload the image to Cloud Storage.
      var filePath = currentUser.uid + '/' + data.key + '/' + file.name;
      return this.storage.ref(filePath).put(file).then(function(snapshot) {

        // Get the file's Storage URI and update the chat message placeholder.
        var fullPath = snapshot.metadata.fullPath;
        return data.update({imageUrl: this.storage.ref(fullPath).toString()});
      }.bind(this));
    }.bind(this)).catch(function(error) {
      console.error('There was an error uploading a file to Cloud Storage:', error);
    });
  }
};

// Signs-in Friendly Chat.
StereoGallery.prototype.signIn = function() {
  // Sign in Firebase using popup auth and Google as the identity provider.
  var provider = new firebase.auth.GoogleAuthProvider();
  this.auth.signInWithPopup(provider);
};

// Signs-out of Friendly Chat.
StereoGallery.prototype.signOut = function() {
  // Sign out of Firebase.
  this.auth.signOut();
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
StereoGallery.prototype.onAuthStateChanged = function(user) {
  if (user) { // User is signed in!
    // Get profile pic and user's name from the Firebase user object.
    var profilePicUrl = user.photoURL;
    var userName = user.displayName;

    // Set the user's profile pic and name.
    this.userPic.style.backgroundImage = 'url(' + (profilePicUrl || '/images/profile_placeholder.png') + ')';
    this.userName.textContent = userName;

    // Show user's profile and sign-out button.
    this.userName.removeAttribute('hidden');
    this.userPic.removeAttribute('hidden');
    this.signOutButton.removeAttribute('hidden');

    // Hide sign-in button.
    this.signInButton.setAttribute('hidden', 'true');

    // We load currently existing chant messages.
      this.loadMessages();
      this.initPage();
  } else { // User is signed out!
    // Hide user's profile and sign-out button.
    this.userName.setAttribute('hidden', 'true');
    this.userPic.setAttribute('hidden', 'true');
    this.signOutButton.setAttribute('hidden', 'true');

    // Show sign-in button.
    this.signInButton.removeAttribute('hidden');
  }
};

// Returns true if user is signed-in. Otherwise false and displays a message.
StereoGallery.prototype.checkSignedInWithMessage = function() {
  // Return true if the user is signed in Firebase
  if (this.auth.currentUser) {
    return true;
  }

  // Display a message to the user using a Toast.
  var data = {
    message: 'You must sign-in first',
    timeout: 2000
  };
    this.showSnackbar(data.message, data.timeout);
  return false;
};

StereoGallery.prototype.showSnackbar = function(msg, duration=3500) {
    this.signInSnackbar.MaterialSnackbar.showSnackbar({ message: msg, timeout: duration });
};


// Resets the given MaterialTextField.
StereoGallery.resetMaterialTextfield = function(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
};

// Template for messages.
StereoGallery.MESSAGE_TEMPLATE =
    '<div class="message-container">' +
      '<div class="spacing"><div class="pic"></div></div>' +
      '<div class="message"></div>' +
      '<div class="name"></div>' +
    '</div>';

StereoGallery.INPUT_TEMPLATE =

//  <div class="card-content">
//  '<div class="mdl-card mdl-cell mdl-cell--12-col">' +
//  '<div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">' +
//  '<h4 class="mdl-cell mdl-cell--12-col">Details</h4>' +
  //   <div class="cafe-header">Cafe Basilico
  //     <div class="cafe-location cafe-light">
  //       <iron-icon icon="communication:location-on"></iron-icon>
  //       <span>250ft</span>
  //     </div>
  //   </div>
  //   <div class="cafe-rating">
  //     <iron-icon class="star" icon="star"></iron-icon>
  //     <iron-icon class="star" icon="star"></iron-icon>
  //     <iron-icon class="star" icon="star"></iron-icon>
  //     <iron-icon class="star" icon="star"></iron-icon>
  //     <iron-icon class="star" icon="star"></iron-icon>
  //   </div>
  //   <p>$・Italian, Cafe</p>
  //   <p class="cafe-light">Small plates, salads &amp; sandwiches in an intimate setting.</p>
  // </div>
  // <div class="card-actions">
  //   <div class="horizontal justified">
  //     <paper-icon-button icon="icons:event"></paper-icon-button>
  //     <paper-button>5:30PM</paper-button>
  //     <paper-button>7:30PM</paper-button>
  //     <paper-button>9:00PM</paper-button>
  //     <paper-button class="cafe-reserve">Reserve</paper-button>
  //   </div>
  // </div>

  `<paper-card image="" id="card">
<paper-input label="Image url" id="url"></paper-input>
  <paper-input label="Description" id="description"></paper-input>
  <paper-input label="attribution" id="attribution"></paper-input>
  <br />
  <paper-button class="mdl-color-text--white mdl-color--light-blue-700" id="remove-inputs" raised>Remove item</paper-button>
</paper-card>
`;
//</div></div>';

/**
 * Div is the div to add this into. Record is the stereograph
 * record to add in as well.
 */
StereoGallery.prototype.addInputNode = function(record) {
  let container = document.createElement('div');
  container.innerHTML = StereoGallery.INPUT_TEMPLATE;
  container.setAttribute('class', 'inputset mdl-shadow--2dp mdl-cell mdl-cell--8-col mdl-card mdl-card__supporting-text');
  componentHandler.upgradeElement(container);
  this.allInputs.appendChild(container);
  // When the user clicks in a new field, add an input to the end of
  // the list if necessary.  TODO(davidquaid): Would it be better to
  // just have insert/remove buttons for every element?
  let url = container.querySelector('#url');
  url.addEventListener(
    'focus', this.addInputIfNecessary.bind(this));
  url.addEventListener(
    'input',
    function(event) {
      container.querySelector('#card').setAttribute('image', event.srcElement.value);
    });
  let description = container.querySelector('#description');
  description.addEventListener(
    'focus', this.addInputIfNecessary.bind(this));
  let attribution = container.querySelector('#attribution');
  attribution.addEventListener(
    'focus', this.addInputIfNecessary.bind(this));

  let removeButton = container.querySelector('#remove-inputs');
  if (this.allInputs.children.length == 1) {
    removeButton.enabled = false;
  } else {
    removeButton.addEventListener('click', function(event) {
      this.allInputs.removeChild(container);
    }.bind(this));
  }
  if (record != null) {
    url.value = record.url;
    container.querySelector('#card').setAttribute('image', url.value);
    description.value = record.description;
    attribution.value = record.attribution;
  }
};

StereoGallery.prototype.addInputIfNecessary = function(event) {
    let inputSets = this.allInputs.querySelectorAll('.inputset');
    if (inputSets.length == 0
	|| (event !== null
	    && inputSets[inputSets.length - 1] === event.target.parentElement.parentElement)) {
	this.addInputNode();
    }
}

// A loading image URL.
StereoGallery.LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif';

// Displays a Message in the UI.
StereoGallery.prototype.displayMessage = function(key, name, text, picUrl, imageUri) {
  var div = document.getElementById(key);
  // If an element for that message does not exists yet we create it.
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = StereoGallery.MESSAGE_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', key);
    this.messageList.appendChild(div);
  }
  if (picUrl) {
    div.querySelector('.pic').style.backgroundImage = 'url(' + picUrl + ')';
  }
  div.querySelector('.name').textContent = name;
  var messageElement = div.querySelector('.message');
  if (text) { // If the message is text.
    messageElement.textContent = text;
    // Replace all line breaks by <br>.
    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  } else if (imageUri) { // If the message is an image.
    var image = document.createElement('img');
    image.addEventListener('load', function() {
      this.messageList.scrollTop = this.messageList.scrollHeight;
    }.bind(this));
    this.setImageUrl(imageUri, image);
    messageElement.innerHTML = '';
    messageElement.appendChild(image);
  }
  // Show the card fading-in and scroll to view the new message.
  setTimeout(function() {div.classList.add('visible')}, 1);
  this.messageList.scrollTop = this.messageList.scrollHeight;
  this.messageInput.focus();
};

// Enables or disables the submit button depending on the values of the input
// fields.
StereoGallery.prototype.toggleButton = function() {
  if (this.messageInput.value) {
    this.submitButton.removeAttribute('disabled');
  } else {
    this.submitButton.setAttribute('disabled', 'true');
  }
};

// Checks that the Firebase SDK has been correctly setup and configured.
StereoGallery.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !window.config) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions.');
  } else if (config.storageBucket === '') {
    window.alert('Your Cloud Storage bucket has not been enabled. Sorry about that. This is ' +
        'actually a Firebase bug that occurs rarely. ' +
        'Please go and re-generate the Firebase initialisation snippet (step 4 of the codelab) ' +
        'and make sure the storageBucket attribute is not empty. ' +
        'You may also need to visit the Storage tab and paste the name of your bucket which is ' +
        'displayed there.');
  }
};

window.onload = function() {
  window.stereoGallery = new StereoGallery();
};
