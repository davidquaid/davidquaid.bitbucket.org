let index = 0;
let Z_MAX = 4;
let Z_MIN = -1;
let DELTA = .3;
AFRAME.registerComponent('sg', {
    schema: {type: 'string'},
    init: function () {
	var stringToLog = this.data;
	console.log(stringToLog);

	let l  = document.querySelector('#l');
	let r  = document.querySelector('#r');
	let cursor = document.querySelector('#cursor');
	l.addEventListener('click', this.advanceCard.bind(this));
	l.addEventListener('mouseenter', function(event) {
	    // Keep the cursor invisible over the left eye.
	    cursor.setAttribute('visible', false);
	    // Make sure the right eye gets the memo about the mouse movement!
	    r.dispatchEvent(new MouseEvent(event.type, event));
	});
	l.addEventListener('mouseleave', function(event) {
	    r.dispatchEvent(new MouseEvent(event.type, event));
	});
	l.addEventListener('mouseleave', function(event) { cursor.setAttribute('visible', true); });
	document.querySelector('#zoom_in').addEventListener('click', this.zoom_in.bind(this));
	document.querySelector('#zoom_out').addEventListener('click', this.zoom_out.bind(this));
	document.querySelector('#prev').addEventListener('click', this.prev.bind(this));
	document.querySelector('#next').addEventListener('click', this.next.bind(this));

	// Okay, parse the params!
	let queryParams = getJsonFromUrl(false);
	if (queryParams.id) {
	    let id = queryParams.id;
	    let database = firebase.database();
	    let galleryRef = database.ref(`galleries/${id}`);
	    galleryRef.once('value').then(function (lookupResult) {
		if (lookupResult.val()) {
		    this.setupCollection(lookupResult.val());
		} else {
		    console.log(`Invalid id: ${lookupResult.getKey()}`);
		}
	    }.bind(this));
	}

	// TODO(davidquaid): This should not be necessary, but the initial description
	// is missing without it.
	//setTimeout(function() {	this.advanceCard(); }.bind(this), 1000);
    },
    setupCollection: function(collection) {
	let title = document.querySelector('#title');
	title.setAttribute('value', collection.title);
	this.dataArray = collection.records;
	this.advanceCard();
    },
    advanceCard: function(event) {
	this.advanceBy(1);
    },
    advanceBy: function(offset) {
	let canvasL = document.querySelector('#left-canvas');
	let canvasR = document.querySelector('#right-canvas');

	let description = document.querySelector('#description');
	let card = this.dataArray[index];
	let adjustedIndex = index + offset;
	index = ((adjustedIndex % this.dataArray.length) + this.dataArray.length) % this.dataArray.length;

	var downloadingImage = new Image();
	downloadingImage.crossOrigin = 'Anonymous';
	downloadingImage.onload = function() {
	    canvasL.height = downloadingImage.height;
	    canvasL.width = downloadingImage.width / 2;
	    canvasR.height = downloadingImage.height;
	    canvasR.width = downloadingImage.width / 2;

	    // draw the left eye:
	    canvasL.getContext('2d').drawImage(downloadingImage, 0, 0, canvasL.width, canvasL.height, 0, 0, canvasL.width, canvasL.height);
	    // Draw the right eye
            canvasR.getContext('2d').drawImage(downloadingImage, canvasR.width, 0, canvasR.width, canvasR.height, 0, 0, canvasR.width, canvasR.height);
	};
	
	downloadingImage.src = card.url.replace('m.jpg', 'b.jpg');

	description.setAttribute('value', (index == 0 ? this.dataArray.length : index).toString() + "/" + this.dataArray.length.toString() + "\n" +  card.description);

    },
    next: function() {
	this.advanceBy(1);
    },
    prev: function() {
	this.advanceBy(-1);
    },
    zoom_in: function(event) {
	this.zoom_by(-DELTA);
    },
    zoom_out: function(event) {
	this.zoom_by(DELTA);
    },
    zoom_by: function(delta) {
	let camera = document.querySelector('a-camera');
	let position = camera.getAttribute('position');
	let newPosition = {x: position.x, y: position.y, z: position.z + delta};
	if (newPosition.z < Z_MAX && newPosition.z > Z_MIN) {
	    camera.setAttribute('position', newPosition);
	}

    }
});

